'use strict'

const AWS = require('aws-sdk')

const fs = require('fs')
const path = require('path')
const { promisify } = require('util')
const readFile = promisify(fs.readFile)


const REGION = 'eu-west-1'

const ses = new AWS.SES({
  region: REGION,
})

const logError = label => error => {
  console.error(label, error)
  return Promise.reject(error)
}

const getTemplate = name => {
  const filepath = path.resolve(__dirname, 'email-templates', `${name}.html`)
  return readFile(filepath, 'utf8')
    .catch(logError(`Error reading file ${filepath}`))
}

const makeParams = (receivers, subject) => body => ({
  Source: 'wengy@test-auto.danielwellington.com',
  Destination: {
    ToAddresses: receivers.map((email = '') => email.trim()),
  },
  Message: {
    Subject: {
      Data: subject,
      Charset: 'UTF-8',
    },
    Body: {
      Html: {
        Data: body,
        Charset: 'UTF-8'
      }
    }
  }
})

const sendEmail = params => {
  console.log('Params ', params)

  ses.sendEmail(params).promise()
    .then(() => 'Email sent')
}


module.exports.send = (receivers, subject, templateName) => {
  return getTemplate(templateName)
    .then(makeParams(receivers, subject))
    .then(sendEmail)
}
