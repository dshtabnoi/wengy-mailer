const { send } = require('./ses-mailer')

const [ TEMPLATE_NAME, TO_EMAILS ] = process.argv.slice(2)


const main = () =>
  send(TO_EMAILS.split(','), 'Test mail', TEMPLATE_NAME)

main()
