const AWS = require('aws-sdk');
const nodemailer = require('nodemailer');

// configure AWS SDK
AWS.config.region = 'eu-west-1'

// create Nodemailer SES transporter
let transporter = nodemailer.createTransport({
    SES: new AWS.SES({
        apiVersion: '2010-12-01'
    }),
    sendingRate: 1,
});

// send some mail
transporter.sendMail({
    from: 'wengy@test-auto.danielwellington.com',
    to: 'denis.shtabnoi@danielwellington.com',
    subject: 'Message',
    text: 'I hope this message gets sent!',
}, (err, info) => {
    console.log('Err', err)
    console.log(info.envelope);
    console.log(info.messageId);
});
